package com.dgpar.codingchallengebackend.entity;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DataLoader implements ApplicationRunner {

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final MemberRepository memberRepository;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Role memberRole = Role.builder()
                .roleName("ROLE_MEMBER")
                .build();

        Role adminRole = Role.builder()
                .roleName("ROLE_ADMIN")
                .build();

        if(roleRepository.findByRoleName(adminRole.getRoleName()).isEmpty()){
            roleRepository.save(adminRole);
        }
        if(roleRepository.findByRoleName(memberRole.getRoleName()).isEmpty()){
            roleRepository.save(memberRole);
        }

        //Predefined admin set up with password "test123"
        Member admin = Member.builder()
                .username("admin")
                .password(passwordEncoder.encode("test123"))
                .role(adminRole)
                .active(1)
                .build();
        if(memberRepository.findByUsername(admin.getUsername()).isEmpty()){
            memberRepository.save(admin);
        }
    }
}
