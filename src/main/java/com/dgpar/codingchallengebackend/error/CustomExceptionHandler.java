package com.dgpar.codingchallengebackend.error;

import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<List<CustomErrorResponse>> handleValidationException(MethodArgumentNotValidException exception){

        List<CustomErrorResponse> errors = new ArrayList<>();

        exception.getBindingResult().getAllErrors().forEach((error ->{
            CustomErrorResponse errorResponse = new CustomErrorResponse();
            errorResponse.setMessage(error.getDefaultMessage());
            errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            errors.add(errorResponse);
        } ));

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<CustomErrorResponse> apiErrorResponse(ResponseStatusException ex) {
        CustomErrorResponse errors = new CustomErrorResponse(ex.getStatusCode().value(), ex.getReason());
        return new ResponseEntity<>(errors, ex.getStatusCode());
    }


    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class CustomErrorResponse {
        private int status;
        private String message;
    }

}
