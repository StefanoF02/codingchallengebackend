package com.dgpar.codingchallengebackend.api.mapper;


import com.dgpar.codingchallengebackend.api.model.MemberCreationDTO;
import com.dgpar.codingchallengebackend.api.model.MemberReturnDTO;
import com.dgpar.codingchallengebackend.entity.Member;
import com.dgpar.codingchallengebackend.entity.Role;
import org.springframework.stereotype.Component;

@Component
public class MemberMapper {
    public Member toDomain(MemberCreationDTO memberCreationDTO, Role defaultRole){
        return Member.builder()
                .firstname(memberCreationDTO.getFirstname())
                .lastname(memberCreationDTO.getLastname())
                .email(memberCreationDTO.getEmail())
                .username(memberCreationDTO.getUsername())
                .password(memberCreationDTO.getPassword())
                .role(defaultRole)
                .active(1)
                .build();
    }

    public MemberReturnDTO toApi(Member member){
        return MemberReturnDTO.builder()
                .firstname(member.getFirstname())
                .lastname(member.getLastname())
                .email(member.getEmail())
                .username(member.getUsername())
                .build();
    }

    public MemberReturnDTO mapCreationToReturn(MemberCreationDTO member){
        return MemberReturnDTO.builder()
                .firstname(member.getFirstname())
                .lastname(member.getLastname())
                .email(member.getUsername())
                .username(member.getUsername())
                .build();
    }
}
