package com.dgpar.codingchallengebackend.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer> {

    Optional<Member> findById(Integer id);

    Optional<Member> findByUsername(String username);

    Member save(Member member);

    void deleteById(Integer id);
}
