package com.dgpar.codingchallengebackend.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role save(Role role);
    Optional<Role> findByRoleName(String roleName);
}
