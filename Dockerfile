FROM quay.io/wildfly/wildfly
ARG STANDALONE=standalone.xml
COPY ${STANDALONE} /opt/jboss/wildfly/standalone/configuration/
WORKDIR /opt/jboss/wildfly/standalone/deployments/
RUN curl -L -o artifacts.zip https://gitlab.com/api/v4/projects/59465502/jobs/artifacts/main/download?job=build --ssl-no-revoke
# change the user to root
USER root
RUN microdnf install unzip -y
# change the user back to jboss
USER jboss
RUN unzip artifacts.zip
ARG WAR_FILE=build/libs/ccbackend.war
ADD ${WAR_FILE} .