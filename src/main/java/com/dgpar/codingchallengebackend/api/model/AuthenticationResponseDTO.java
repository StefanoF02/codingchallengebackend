package com.dgpar.codingchallengebackend.api.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class AuthenticationResponseDTO {
    private String token;
}
