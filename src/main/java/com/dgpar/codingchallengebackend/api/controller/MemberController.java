package com.dgpar.codingchallengebackend.api.controller;

import com.dgpar.codingchallengebackend.api.model.AuthenticationResponseDTO;
import com.dgpar.codingchallengebackend.api.model.CredentialsDTO;
import com.dgpar.codingchallengebackend.api.model.MemberCreationDTO;
import com.dgpar.codingchallengebackend.api.model.MemberReturnDTO;
import com.dgpar.codingchallengebackend.api.service.MemberService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class MemberController {
    @Autowired
    private final MemberService memberService;


    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponseDTO> login(@RequestBody CredentialsDTO credentialsDTO) {
        return new ResponseEntity<>(new AuthenticationResponseDTO(memberService.login(credentialsDTO)), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<MemberReturnDTO> register(@RequestBody MemberCreationDTO memberCreationDTO) {
        return new ResponseEntity<>(memberService.register(memberCreationDTO), HttpStatus.OK);
    }

    @GetMapping("/thisIsProtected")
    public ResponseEntity<HttpStatus> protectedRoute() {
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/thisIsSuperProtected")
    public ResponseEntity<HttpStatus> superProtectedRoute() {
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/members/{id}")
    public ResponseEntity<MemberReturnDTO> getMember(@PathVariable("id") Integer id) {
        return memberService.getMemberById(id) == null
                ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                : new ResponseEntity<>(memberService.getMemberById(id), HttpStatus.OK);
    }

    //TODO: Adding 404 if member not found
    @DeleteMapping("/members/{id}")
    public ResponseEntity<HttpStatus> deleteMember(@PathVariable("id") Integer id) {
        memberService.deleteMember(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
