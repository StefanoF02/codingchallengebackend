package com.dgpar.codingchallengebackend.api.service;

import com.dgpar.codingchallengebackend.api.mapper.MemberMapper;
import com.dgpar.codingchallengebackend.api.model.CredentialsDTO;
import com.dgpar.codingchallengebackend.api.model.MemberCreationDTO;
import com.dgpar.codingchallengebackend.api.model.MemberReturnDTO;
import com.dgpar.codingchallengebackend.entity.Member;
import com.dgpar.codingchallengebackend.entity.MemberRepository;
import com.dgpar.codingchallengebackend.entity.RoleRepository;
import com.dgpar.codingchallengebackend.security.JwtService;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberService {

    @Autowired
    private final MemberRepository memberRepository;

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final MemberMapper memberMapper;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final AuthenticationManager authenticationManager;

    @Autowired
    private final JwtService jwtService;

    public String login(CredentialsDTO credentialsDTO) {
        Optional<Member> existingMember = memberRepository.findByUsername(credentialsDTO.getUsername());
        if (existingMember.isPresent()) {
            Member member = existingMember.get();
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                        credentialsDTO.getUsername(),
                        credentialsDTO.getPassword()));
            } catch (AuthenticationException e) {
                //In real application repsonse messages should not differ. Credential stuffing.
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Authentication failure.");
            }
            return jwtService.generateToken(member);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown combination of username and password.");
        }
    }

    public MemberReturnDTO register(MemberCreationDTO newMember) {
        newMember.setPassword(passwordEncoder.encode(newMember.getPassword()));
        memberRepository.save(memberMapper.toDomain(newMember, roleRepository.findByRoleName("ROLE_MEMBER").get()));
        return memberMapper.mapCreationToReturn(newMember);
    }

    public @Nullable MemberReturnDTO getMemberByUsername(@NonNull String username) {
        Optional<Member> persistedMember = memberRepository.findByUsername(username);
        return persistedMember.orElse(null) != null
                ? memberMapper.toApi(persistedMember.get())
                : null;
    }


    public @Nullable MemberReturnDTO getMemberById(@NonNull Integer id) {
        Optional<Member> persistedMember = memberRepository.findById(id);
        return persistedMember.orElse(null) != null
                ? memberMapper.toApi(persistedMember.get())
                : null;
    }

    public void deleteMember(Integer id) {
        memberRepository.deleteById(id);
    }
}
