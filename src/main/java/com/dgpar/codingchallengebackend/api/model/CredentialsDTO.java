package com.dgpar.codingchallengebackend.api.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CredentialsDTO {
    private String username;
    private String password;
}
