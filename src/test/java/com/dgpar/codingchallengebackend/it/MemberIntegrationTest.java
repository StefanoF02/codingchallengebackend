package com.dgpar.codingchallengebackend.it;

import com.dgpar.codingchallengebackend.api.model.MemberCreationDTO;
import com.dgpar.codingchallengebackend.api.model.MemberReturnDTO;
import com.dgpar.codingchallengebackend.api.service.MemberService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@Testcontainers(disabledWithoutDocker = true)
public class MemberIntegrationTest {

    @LocalServerPort
    private Integer port;

    @Autowired
    private MemberService memberService;


    @Container
    static PostgreSQLContainer<?> postgres =
            new PostgreSQLContainer<>("postgres:16-alpine")
                    .withDatabaseName("TestDB")
                    .withUsername("TestUser")
                    .withPassword("1234");

    @BeforeAll
    static void beforeAll(){
        postgres.start();
    }

    @AfterAll
    static void afterAll(){
        postgres.stop();
    }

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.driver-class-name", postgres::getDriverClassName);
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }


    @Test
    public void saveMember(){
        MemberCreationDTO member = MemberCreationDTO.builder()
                .firstname("TestFirstName")
                .lastname("TestLastName")
                .email("TestMail")
                .username("TestUserName")
                .password("test12345678")
                .build();

        memberService.register(member);

        MemberReturnDTO memberReturn=  memberService.getMemberByUsername(member.getUsername());

        assertThat(member.getFirstname()).isEqualTo(memberReturn.getFirstname());
        assertThat(member.getLastname()).isEqualTo(memberReturn.getLastname());
        assertThat(member.getEmail()).isEqualTo(memberReturn.getEmail());
        assertThat(member.getUsername()).isEqualTo(memberReturn.getUsername());

    }


}
