package com.dgpar.codingchallengebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CodingChallengeBackendApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
		return application.sources(CodingChallengeBackendApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(CodingChallengeBackendApplication.class, args);
	}

}
