package com.dgpar.codingchallengebackend.api.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MemberReturnDTO {
    private String firstname;

    private String lastname;

    private String email;

    private String username;
}
